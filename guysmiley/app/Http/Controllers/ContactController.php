<?php

namespace App\Http\Controllers;

use Mail;
use App\Contact;
use Illuminate\Http\Request;
use App\Mail\ContactEmail;

class ContactController extends Controller
{

    public function handleContact(Request $request) {
        return view('contact');
    }

    public function processContact(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'phone' => 'nullable',
        ]);

        $contact = new Contact();
        $contact->name = $validatedData['name'];
        $contact->email = $validatedData['email'];
        $contact->message = $validatedData['message'];
        $contact->phone = $validatedData['phone'];
        $contact->save();

        Mail::to('guy-smiley@example.com')->send(new ContactEmail($contact));


        $request->session()->flash('status', 'Thank you for your feedback.');

        return response()->redirectTo('/');
    }
}
