<?php

namespace Tests\Feature;

use Mail;
use App\Contact;
use App\Mail\ContactEmail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactEmailTest extends TestCase
{
    /**
     * Tests sending a contact email.
     *
     * @return void
     */
    public function testCreateContactEmail()
    {
        Mail::fake();

        $contact = new Contact();
        $contact->name = 'Test User';
        $contact->email = 'user@host.tld';
        $contact->message = 'Test Message';
        $contact->phone = '1-800-555-1024';
        $contact->save();
        $contact->fresh();

        Mail::to('test@host.tld')->send(new ContactEmail($contact));
        Mail::assertSent(ContactEmail::class, 1);

    }
}
