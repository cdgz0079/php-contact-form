<?php

namespace Tests\Feature;

use App\Contact;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactModelTest extends TestCase
{
    /**
     * Creates a new Contact object then saves object. Checks if the object exists in the DB.
     *
     * @return void
     */
    public function testCreateContact()
    {
        $testContact = new Contact();

        $testContact->name = 'Test Name';
        $testContact->email = 'user@host.tld';
        $testContact->phone = '206-882-2040';
        $testContact->message = 'Hello, thats a nice Tnetennba.';
        $testContact->save();
        $testContact->fresh();
        // Id is set when saving, but not refreshed in the model

        $retrievedContact = Contact::where('id', $testContact->id)->first();

        $this->assertEquals($retrievedContact->toArray(), $testContact->toArray());
    }

}
